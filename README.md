# TelegramMe

Notificações via telegram.

![alt text](result.png "Conversa com o bot com a mensagem enviada por ele")

Com o script send.sh você pode enviar qualquer mensagem para o chat configurado no script.

O script get_ip.sh obtém o IP atráves do https://ipecho.net e chama o send.sh para enviar.

### Como usar
1. Crie um novo bot através do https://t.me/BotFather
2. Copie para o arquivo send.sh o token do seu bot
3. Inicie uma conversa com seu bot ou crie um grupo e o adicione
4. Envie /start para ele ou no grupo
5. Acesse https://api.telegram.org/bot[TOKEN]/getUpdates
6. Copie o ID do chat para onde o bot irá enviar as mensagens para o script send.sh

Agora você pode adicionar ao crontab ou colocar em algum hook de aplicação como no exemplo da referência que envia uma mensagem quando alguém conecta via SSH.

### Referência:
https://bogomolov.tech/Telegram-notification-on-SSH-login/
